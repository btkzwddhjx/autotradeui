﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APITest
{
    enum State
    {
        ABOVE,
        BELOW
    };

    class SysMan
    {
        private State _state;
        private int _counter;
        private int _position;
        private int _buyprice;
        private int _profit;
        private Timer _timer;
        private Instrument _Instr;
        private List<int> _list;
        private bool _first;
        private int _qty;

        public event OnFillEventHandler OnFill;

        public SysMan()
        {
            _first = true;
            _qty = 1;

            _list = new List<int>();

            _Instr = new Instrument();
            //_Instr.OnPriceUpdate += _Instr_OnPriceUpdate;
            _Instr.OnFill += _Instr_OnFill;

            _timer = new Timer();
            _timer.Tick += _timer_Tick;
            _timer.Interval = 5000;
            _timer.Enabled = true;
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            _list.Add(_Instr.Last);

            if (_counter > 10)
            {
                _list.RemoveAt(0);

                int sum = 0;
                foreach (int p in _list)
                {
                    sum += p;
                }
                double avg = sum / 10;

                if (_first)
                {
                    if (_Instr.Last > avg)
                    {
                        _state = State.ABOVE;
                    }
                    else
                    {
                        _state = State.BELOW;
                    }
                    _first = false;
                }

                if (_Instr.Last > avg && _state == State.BELOW)
                {
                    //BUY
                    _Instr.SendMarketOrder("B", _qty.ToString());
                    _state = State.ABOVE;
                    _qty = 2;
                }
                if (_Instr.Last < avg && _state == State.ABOVE)
                {
                    //SELL
                    _Instr.SendMarketOrder("S", _qty.ToString());
                    _state = State.BELOW;
                    _qty = 2;
                }
                //_qty = 2;
            }
            _timer.Interval = 1000;

        }

        public int Last { get { return _Instr.Last; } }
        public State MyState { get { return _state; } }
        //public double MA { get { return avg; } }


        void _Instr_OnFill(string[] data)
        {
            if (data[1] == "B")
            {
                _position += Convert.ToInt32(data[3]);
                //_buyprice = Convert.ToInt32(data[2]);
            }
            else 
            {
                _position -= Convert.ToInt32(data[3]);
                //_profit += (Convert.ToInt32(data[2]) - _buyprice) * Convert.ToInt32(data[3]);
            }
            OnFill(data);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace APITest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private SysMan _SysMan;
        private Timer _Timer;

        private void button1_Click(object sender, EventArgs e)
        {
            _SysMan = new SysMan();
            _SysMan.OnFill += SysMan_OnFill;

            _Timer = new Timer();
            _Timer.Tick += _Timer_Tick;
            _Timer.Interval = 5000;
            _Timer.Enabled = true;
        }
        void _Timer_Tick(object sender, EventArgs e)
        {
            textBox7.Text = _SysMan.Last.ToString();
            //textBox8.Text = _SysMan.MA.ToString();
            textBox9.Text = _SysMan.MyState.ToString();

            if (_SysMan.Last != 0)
            {
                chart1.Visible = true;

                chart1.Series[0].Points.AddXY(DateTime.Now.ToOADate(), _SysMan.Last / 100.0);

                double max = Math.Ceiling(chart1.Series[0].Points.FindMaxByValue("Y1").YValues[0]);
                double min = Math.Floor(chart1.Series[0].Points.FindMinByValue("Y1").YValues[0]);

                chart1.ChartAreas[0].AxisY.Maximum = max + 1.0;
                chart1.ChartAreas[0].AxisY.Minimum = min - 1.0;

                chart1.ChartAreas[0].AxisY.Interval = 1.0;
                chart1.ChartAreas[0].AxisX.Interval = 15;

                double removetime = DateTime.Now.AddSeconds(-180.0).ToOADate();

                if (chart1.Series[0].Points[0].XValue < removetime)
                {
                    chart1.Series[0].Points.RemoveAt(0);
                }

                chart1.ChartAreas[0].AxisX.Maximum = chart1.Series[0].Points[0].XValue;
                chart1.ChartAreas[0].AxisX.Minimum = DateTime.FromOADate(chart1.Series[0].Points[0].XValue).AddSeconds(185.0).ToOADate();
                chart1.Invalidate();

            }
        }

        void SysMan_OnFill(string[] data)
        {
            textBox10.Text += "FILL RECEIVED: " + data[0] + "\t" + data[1] + "\t" + data[3] + " contract(s) @\t" + data[2] + Environment.NewLine;
            //textBox9.Text = _profit.ToString();
        }

        //void _Instr_OnPriceUpdate(Instrument pInstr)
        //{
            //textBox1.Text = pInstr.BidQty.ToString();
            //textBox2.Text = pInstr.Bid.ToString();
            //textBox3.Text = pInstr.Ask.ToString();
            //textBox4.Text = pInstr.AskQty.ToString();
            //textBox5.Text = pInstr.Last.ToString();
            //textBox6.Text = pInstr.LastQty.ToString();
        //}
        private void button2_Click(object sender, EventArgs e)
        {
            //_Instr.SendMarketOrder("B", textBox7.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //_Instr.SendMarketOrder("S", textBox7.Text);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //_Instr.SendLimitOrder("B", textBox7.Text, textBox8.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //_Instr.SendLimitOrder("S", textBox7.Text, textBox8.Text);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //_Instr.CancelOrder(textBox9.Text);
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}

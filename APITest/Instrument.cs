﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TraderAPI;

namespace APITest
{
    delegate void OnFillEventHandler( string[] data );
    delegate void OnPriceUpdateEventHandler( Instrument pInstr );
    class Instrument
    {
        private InstrObjClass _Instr;
        private InstrNotifyClass _Notify;
        private OrderSetClass _OrderSet;

        private int _BidQty;
        private int _Bid;
        private int _Ask;
        private int _AskQty;
        private int _Last;
        private int _LastQty;
        
        public event OnPriceUpdateEventHandler OnPriceUpdate;
        public event OnFillEventHandler OnFill;

        public int BidQty { get { return _BidQty; } }
        public int Bid { get { return _Bid; } }
        public int Ask { get { return _Ask; } }
        public int AskQty { get { return _AskQty; } }
        public int Last { get { return _Last; } }
        public int LastQty { get { return _LastQty; } }

        public Instrument()
        {
            _Instr = new InstrObjClass("CME", "ES", InstrObjClass.ProdType.FUTURE, "Dec14", 200000);
            _Notify = (InstrNotifyClass)_Instr.CreateNotifyObj;
            _Notify.EnablePriceUpdates = true;
            _Notify.UpdateFilter = "BIDQTY,BID,ASK,ASKQTY,LAST,LASTQTY";

            _Notify.OnNotifyUpdate += _Notify_OnNotifyUpdate;
            
            _Instr.Open(true);

            _OrderSet = new OrderSetClass();
            _OrderSet.set_Set("MAXORDERS", 1000);
            _OrderSet.set_Set("MAXORDERQTY", 1000);
            _OrderSet.set_Set("MAXWORKING", 1000);
            _OrderSet.set_Set("MAXPOSITION", 1000);
            _OrderSet.EnableOrderAutoDelete = true;
            _OrderSet.EnableOrderSend = true;
            _OrderSet.EnableOrderFillData = true;
            _OrderSet.OnOrderFillData += _OrderSet_OnOrderFillData;
            _OrderSet.Open(true);
            _Instr.OrderSet = _OrderSet;
        }

        public void CancelOrder(string Key)
        {
            _OrderSet.Cancel(Key);
        }
               
        void _OrderSet_OnOrderFillData(FillObj pFill)
        {
            string[] data = new string[5];
            data[0] = pFill.get_Get("KEY").ToString();
            data[1] = pFill.get_Get("BUYSELL").ToString();
            data[2] = pFill.get_Get("PRICE").ToString();
            data[3] = pFill.get_Get("QTY").ToString();
            data[4] = pFill.get_Get("FFT3").ToString();

            OnFill(data);
        }

        void _Notify_OnNotifyUpdate(InstrNotifyClass pNotify, InstrObjClass pInstr)
        {
            _BidQty = Convert.ToInt32( pInstr.get_Get("BIDQTY") );
            _Bid = Convert.ToInt32( pInstr.get_Get("BID") );
            _Ask = Convert.ToInt32( pInstr.get_Get("ASK"));
            _AskQty = Convert.ToInt32( pInstr.get_Get("ASKQTY"));
            _Last = Convert.ToInt32( pInstr.get_Get("LAST"));
            _LastQty = Convert.ToInt32( pInstr.get_Get("LASTQTY"));

            //OnPriceUpdate( this );
        }
        
        public void SendMarketOrder(string BS, string Qty)
        {
            OrderProfileClass _Profile = new OrderProfileClass();
            _Profile.Instrument = _Instr;
            _Profile.set_Set("ACCT", "12345");
            _Profile.set_Set("BUYSELL", BS);
            _Profile.set_Set("ORDERTYPE", "M");
            _Profile.set_Set("ORDERQTY", Qty);
            _Profile.set_Set("FFT3", "MY ORDER");

            _OrderSet.SendOrder(_Profile);
        }

        public void SendLimitOrder(string BS, string Qty, string Price)
        {
            OrderProfileClass _Profile = new OrderProfileClass();
            _Profile.Instrument = _Instr;
            _Profile.set_Set("ACCT", "12345");
            _Profile.set_Set("BUYSELL", BS);
            _Profile.set_Set("ORDERTYPE", "L");
            _Profile.set_Set("LIMIT", Price);
            _Profile.set_Set("ORDERQTY", Qty);
            _Profile.set_Set("FFT3", "MY ORDER");

            _OrderSet.SendOrder(_Profile);
        }
    }
}
